#include <iostream>
#include <algorithm>
#include <random>

#include "F3drl/Core"

struct std_rand_warpper
{
    std_rand_warpper() : gen(rd()) { }
    inline std::size_t operator () (){ return dis(gen); }

    std::random_device rd;
    std::mt19937_64 gen;
    std::uniform_int_distribution<std::size_t> dis;
};

template<typename iterator>
inline void do_terator_test(iterator &it)
{
    it.reset();
    while(++it)
    {
        std::cout <<
            it[0] << "," <<
            it[1] << "," <<
            it[2] << std::endl;
    }
}

int main(int argc, const char *argv[])
{
    enum { Count = 12 };

    std::vector<std::size_t> indices(Count);
    std::iota(std::begin(indices), std::end(indices), 0);

    std::cout << "index iterator:" << std::endl;
    f3drl::IndexIterator<3> it(Count);
    do_terator_test(it);

    std::cout << "random iterator:" << std::endl;
    f3drl::RandomIterator<3, std_rand_warpper> rd(Count);
    do_terator_test(rd);

    std::cout << "triangle iterator:" << std::endl;
    f3drl::TriangleIterator<3> tr(indices);
    do_terator_test(tr);

    std::cout << "random triangle iterator:" << std::endl;
    f3drl::RandomTriangleIterator<3, std_rand_warpper> rtr(indices, Count/3);
    do_terator_test(rtr);
}
