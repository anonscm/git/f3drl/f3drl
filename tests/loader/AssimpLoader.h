#ifndef __ASSIMP_MESH_LOADER__
#define __ASSIMP_MESH_LOADER__

#include "F3drl/Core"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/mesh.h>
#include <assimp/DefaultLogger.hpp>
#include <assimp/LogStream.hpp>

template<
    typename InputSetType,
    template<class, template <class, unsigned> class _> class Accelerator,
    template<class, unsigned> class DistanceEstimator
>
class AssimpMesh : public Accelerator<InputSetType, DistanceEstimator>
{
    public:
        using Base = Accelerator<InputSetType, DistanceEstimator>;
        using BaseBase = typename Base::Base;
    public:
        AssimpMesh() = default;
        AssimpMesh(const char *filename){ load(filename); }

        inline bool load(const char *filename) noexcept
        {
            BaseBase::points.clear();
            auto scene = importer.ReadFile(filename, aiProcess_Triangulate);

            if(!scene)
                return (isloaded=false);

            for(unsigned int m = 0; m<scene->mNumMeshes; ++m)
            {
                auto &mesh = scene->mMeshes[m];

                if(mesh)
                {
                    unsigned int voffset = BaseBase::points.size();
                    BaseBase::points.resize(voffset + mesh->mNumVertices);

                    for(unsigned int i = 0; i<mesh->mNumVertices; ++i)
                    {
                        BaseBase::points[voffset+i]
                            << mesh->mVertices[i].x,
                               mesh->mVertices[i].y,
                               mesh->mVertices[i].z;
                    }
                }

                if(mesh && mesh->mPrimitiveTypes == aiPrimitiveType_TRIANGLE)
                {
                    unsigned int ioffset = BaseBase::indices.size();
                    BaseBase::indices.resize(ioffset + mesh->mNumFaces*3);

                    for(unsigned int i = 0; i<mesh->mNumFaces; ++i)
                        for(unsigned int j = 0; j<mesh->mFaces[i].mNumIndices; ++j)
                            BaseBase::indices[ioffset+i*mesh->mFaces[i].mNumIndices+j] =
                                mesh->mFaces[i].mIndices[j];
                }
            }

            return (isloaded=true);
        }

        inline void print() const  noexcept
        {
            for(auto v : BaseBase::points)
            {
                std::cout << "v "
                          << v(0,0) << " "
                          << v(1,0) << " "
                          << v(2,0) << std::endl;
            }

            for(unsigned int i = 0; i<BaseBase::indices.size(); i += 3)
            {
                std::cout << "f "
                          << BaseBase::indices[i+0] << " "
                          << BaseBase::indices[i+1] << " "
                          << BaseBase::indices[i+2] << std::endl;
            }
        }

    public:
        Assimp::Importer importer;
        bool isloaded;
};

#endif
