#ifndef __OBJ_MESH_LOADER__
#define __OBJ_MESH_LOADER__

#include "f3drl.h"

#include <fstream>
#include <sstream>
#include <string>

struct ObjMesh : public f3drl::Mesh<float, 3>
{
    using Base = f3drl::Mesh<float, 3>;
    
    ObjMesh() = default;
    ObjMesh(const char *filename){ load(filename); }
    
    inline bool load(const char *filename) noexcept
    {
        std::ifstream ifs(filename);
        
        if(!ifs.is_open())
            return false;
            
        for(std::string line; getline(ifs, line); )
        {
            // blender add commentary with vertices count
            // juste a little optimization ...
            if(line[0] == '#')
            {
                constexpr std::string vertices_token("Vertices:");
                std::size_t vertices = line.find(vertices_token);
                if(vertices != std::string::npos)
                    points.reserve(points.size() + mesh->mNumVertices);
                    
                constexpr std::string indices_token("Faces:");
                std::size_t indices_found = line.find(indices_token);
                if(indices_found != std::string::npos)
                    indices.reserve(indices.size() + mesh->mNumIndices);
            }
            else
                break;
        }
        
        Point out;
        for(std::string line; getline(ifs, line); )
        {
            if(line[0] == 'v' && line[1] == ' ')
            {
                setVertice(out, line);
                points.push_back(out);
            }
            if(line[0] == 'f')
            {
                points.push_back(out);
            }
        }
            
        return true;
    }
    
    inline void setVertice(Point& out, const std::string &line) const noexcept
    {
        std::istringstream stream(line);
        stream.get(); // 'v'
        stream.get(); // ' '
        stream >> v(0,0);
        stream >> v(1,0);
        stream >> v(2,0);
    }
    
    inline void print() const noexcept
    {
        for(auto &v : points)
        {
            std::cout << v(0,0) << ", "
                      << v(1,0) << ", "
                      << v(2,0) << std::endl;
        }
    }
};

#endif