#include <iostream>
#include <algorithm>
#include <random>

#include "F3drl/Core"
#include "loader/AssimpLoader.h"

struct std_rand_warpper
{
    std_rand_warpper() : gen(rd()) { }
    inline std::size_t operator () (){ return dis(gen); }

    std::random_device rd;
    std::mt19937_64 gen;
    std::uniform_int_distribution<std::size_t> dis;
};

struct MyTransformCallback
{
    double estimation;
    Eigen::Affine3f best;

    MyTransformCallback()
        : estimation(std::numeric_limits<double>::infinity())
    {
    }

    bool operator() (const Eigen::Affine3f &tr, double cost)
    {
        if(cost < estimation)
        {
            estimation = cost;
            best = tr;
            // python syntaxe for blender
            std::cout << "Matrix(("
                      << "(" << best(0,0) << "," << best(0,1) << "," << best(0,2) << "," << best(0,3) << "),"
                      << "(" << best(1,0) << "," << best(1,1) << "," << best(1,2) << "," << best(1,3) << "),"
                      << "(" << best(2,0) << "," << best(2,1) << "," << best(2,2) << "," << best(2,3) << "),"
                      << "(" << best(3,0) << "," << best(3,1) << "," << best(3,2) << "," << best(3,3) << ")"
                      << "))";

            std::cout << "\nestimation: " << estimation << std::endl;
        }

        return false;
    }
};

enum { Dim = 3 };
using Scalar = float;
using InputSet = f3drl::InputSet<Scalar, Dim>;

using Base   = AssimpMesh<InputSet, f3drl::ClosestLinearSearch, f3drl::EuclidianDistance>;
using Target = AssimpMesh<InputSet, f3drl::ClosestLinearSearch, f3drl::EuclidianDistance>;

using TransformEstimator = f3drl::TransformEstimator<Scalar, Dim>;
using TransformEvaluator = f3drl::TransformLCPEvaluator<Base, Target, f3drl::EuclidianDistance>;
using PIterator = f3drl::RandomTriangleIterator<Dim, std_rand_warpper>;
using QIterator = f3drl::TriangleIterator<Dim>;

using Solver = f3drl::Ransac<
    Base, Target,
    TransformEstimator,
    TransformEvaluator,
    MyTransformCallback,
    PIterator,
    QIterator
>;

int main(int argc, const char *argv[])
{
    if(argc != 3)
    {
        std::cerr << argv[0] << "filename1 filename2" << std::endl;
        return 1;
    }

    std::cerr << "loading files ..." << std::endl;

    Base p(argv[1]);
    Target q(argv[2]);

    assert(p.isloaded);
    assert(q.isloaded);

    std::cerr << "initializing ..." << std::endl;

    MyTransformCallback tmp;
    TransformEvaluator eval(0.015f);

    PIterator pit(p.indices, 30);
    QIterator qit(q.indices);

    Solver solver(p, q, tmp, eval, pit, qit);

    std::cerr << "solving:" << std::endl;
    solver();

    std::cerr << " done" << std::endl;

    return 0;
}
