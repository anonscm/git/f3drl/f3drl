#ifndef __SUPER4PCS__
#define __SUPER4PCS__

#include "disablewarnings.h"

#include <vector>
#include <limits>

#include "utils/timer.h"
#include "utils/fastrandom.h"

#include "base_iterator/IndexIterator.h"
#include "base_iterator/TriangleIterator.h"
#include "base_iterator/RandomIterator.h"
#include "base_iterator/RandomTriangleIterator.h"

#include "estimator/EuclideanDistance.h"
#include "estimator/TransformEstimator.h"
#include "estimator/TransformEvaluator.h"
#include "estimator/TransformLCPEvaluator.h"

#include "accelerator/ClosestLinearSearch.h"

#include "solver/InputSet.h"
#include "solver/Ransac.h"

// bla bla bla ... not yet

#endif