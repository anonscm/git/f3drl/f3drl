#ifndef __F3DRL_CONGRUENT_DISTANCE_ESTIMATOR__
#define __F3DRL_CONGRUENT_DISTANCE_ESTIMATOR__

namespace f3drl
{
    template<class A>
    struct CongruentNotEstimator
    {
        A a;
        
        inline void init(BaseType *set, std::size_t B[Size]) noexcept
        {
            a.init(set, B);
        }
        
        inline bool compare(BaseType *set, std::size_t current[Size]) const noexcept
        {
            return !a.compare(set, current);
        }
    };
    
    template<class A, class B>
    struct CongruentAndEstimator
    {
        A a;
        B b;
        
        inline void init(BaseType *set, std::size_t B[Size]) noexcept
        {
            a.init(set, B);
            b.init(set, B);
        }
        
        inline bool compare(BaseType *set, std::size_t current[Size]) const noexcept
        {
            return a.compare(set, current) && b.compare(set, current);
        }
    };
    
    template<class A, class B>
    struct CongruentOrEstimator
    {
        A a;
        B b;
        
        inline void init(BaseType *set, std::size_t B[Size]) noexcept
        {
            a.init(set, B);
            b.init(set, B);
        }
        
        inline bool compare(BaseType *set, std::size_t current[Size]) const noexcept
        {
            return a.compare(set, current) || b.compare(set, current);
        }
    };
}