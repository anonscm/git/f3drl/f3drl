#ifndef __F3DRL_CONGRUENT_ANGLE_ESTIMATOR__
#define __F3DRL_CONGRUENT_ANGLE_ESTIMATOR__

namespace f3drl
{
      struct CongruentAngleEstimator
      {
          inline void init(BaseType *set, std::size_t B[Size]) noexcept
          {
          }
          
          inline bool compare(BaseType *set, std::size_t current[Size]) const noexcept 
          {
              return true;
          }
          
          float angle1, angle2, angle3;
      };
}

#endif