#ifndef __F3DRL_CONGRUENT_DISTANCE_ESTIMATOR__
#define __F3DRL_CONGRUENT_DISTANCE_ESTIMATOR__

namespace f3drl
{
      struct CongruentDistanceEstimator
      {
          inline void init(BaseType *set, std::size_t B[Size]) noexcept
          {
              dist1 = EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[B[0]], set->points[B[1]]);
              dist2 = EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[B[2]], set->points[B[1]]);
              dist3 = EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[B[2]], set->points[B[0]]);
          }
          
          inline bool compare(BaseType *set, std::size_t current[Size]) const noexcept 
          {
              return (dist1 == EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[current[0]], set->points[current[1]])) &&
                     (dist2 == EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[current[2]], set->points[current[1]])) &&
                     (dist3 == EuclidianDistance<Scalar, BaseType::Dim>::length(set->points[current[2]], set->points[current[0]]));
          }
          
          float dist1, dist2, dist3;
      };
}

#endif