#ifndef __F3DRL_CONCEPT_EVALUATOR__
#define __F3DRL_CONCEPT_EVALUATOR__

namespace f3drl
{
    class ConceptEvaluator
    {
        public:
            inline double operator() (const BaseType &p, const TargetType &q, const Transform &tr) const noexcept;
    };
}

#endif

