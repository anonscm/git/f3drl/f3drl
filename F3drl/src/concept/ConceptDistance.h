#ifndef __F3DRL_CONCEPT_DISTANCE__
#define __F3DRL_CONCEPT_DISTANCE__

namespace f3drl
{
    template<typename _Scalar, unsigned dimension>
    class ConceptDistance
    {
        public:
            static double length(const Point &A, const Point &B) noexcept;
    };
}

#endif