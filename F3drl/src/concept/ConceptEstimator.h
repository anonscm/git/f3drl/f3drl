#ifndef __F3DRL_CONCEPT_ESTIMATOR__
#define __F3DRL_CONCEPT_ESTIMATOR__

namespace f3drl
{
    struct TransformEstimator
    {
        inline Transform operator() (MatrixIO &in, MatrixIO &out) const noexcept;
    };
}
    
#endif