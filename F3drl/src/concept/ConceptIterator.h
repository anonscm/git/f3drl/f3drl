#ifndef __F3DRL_CONCEPT_ITERATOR__
#define __F3DRL_CONCEPT_ITERATOR__

#include <cstddef>
#include <assert.h>

namespace f3drl
{
    template<unsigned _D = 1u>
    class ConceptIterator
    {
        public:
            inline bool operator++ ();
            inline std::size_t operator[] (const std::size_t i) const;
            inline void reset();
        public:
            std::size_t current[Size];
    };
}

#endif