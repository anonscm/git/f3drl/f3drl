#ifndef __F3DRL_CLOSEST_KDTREE_SEARCH__
#define __F3DRL_CLOSEST_KDTREE_SEARCH__

namespace f3drl
{
    //! typename Mesh must be Super4PCS::Mesh and must be shadowed beffor use
    //! like MyMesh : public Super4PCS::Mesh<float, 3>
    
    template<typename Mesh, typename _Index = int>
    class ClosestKDTreeSearch
    {
        public:
            using Point = typename Mesh::Point;
            using Scalar = typename Mesh::Scalar;
            
            using VectorType = Eigen::Matrix<Scalar,3,1>;
            using AxisAlignedBoxType = AABB3D<Scalar>;
            using NodeList = std::vector<KdNode>;
            using PointList = std::vector<VectorType>;
            using IndexList = std::vector<Index>;

            inline const NodeList&   _getNodes()   { return mNodes;   }
            inline const PointList&  _getPoints()  { return mPoints;  }
            inline const PointList&  _getIndices() { return mIndices; }
            static constexpr Index invalidIndex()  { return -1; }
            
            struct KdNode
            {
                union {
                    struct {
                        float splitValue;
                        unsigned int firstChildId:24;
                        unsigned int dim:2;
                        unsigned int leaf:1;
                    };
                    struct {
                        unsigned int start;
                        unsigned short size;
                    };
                };
            };
        public:
            ClosestKDTreeSearch(const Mesh &mesh,
               unsigned int nofPointsPerCell = KD_POINT_PER_CELL,
               unsigned int maxDepth = KD_MAX_DEPTH
            ) noexcept;
            
            ClosestKDTreeSearch(
                unsigned int size = 0,
                unsigned int nofPointsPerCell = KD_POINT_PER_CELL,
                unsigned int maxDepth = KD_MAX_DEPTH
            ) noexcept;
            
            ~ClosestKDTreeSearch();
            
            template <class VectorDerived>
            inline void add(const VectorDerived &p);
            inline void add(Scalar *position);
            
            inline void finalize();
            inline const AxisAlignedBoxType& aabb() const { return mAABB; }

            template<typename Container = std::vector<VectorType>>
            inline void doQueryDist(const VectorType& queryPoint, Scalar sqdist, Container& result);
            inline void doQueryK(const VectorType& p);

            template<typename IndexContainer = std::vector<Index> >
            inline void doQueryDistIndices(const VectorType& queryPoint, float sqdist, IndexContainer& result);
            inline Index doQueryRestrictedClosestIndex(const VectorType& queryPoint, Scalar sqdist, int currentId = -1);

            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            
            Point find(const Point &other) noexcept;
        protected:
            //! element of the stack
            struct QueryNode
            {
                inline QueryNode() {}
                inline QueryNode(unsigned int id) : nodeId(id) {}
                //! id of the next node
                unsigned int nodeId;
                //! squared distance to the next node
                Scalar sq;
            };

            /*!
              Used to build the tree: split the subset [start..end[ according to dim
              and splitValue, and returns the index of the first element of the second
              subset.
              */
            inline unsigned int split(int start, int end, unsigned int dim, Scalar splitValue);

            void createTree(
                unsigned int nodeId,
                unsigned int start,
                unsigned int end,
                unsigned int level,
                unsigned int targetCellsize,
                unsigned int targetMaxDepth
            );

            /*!
             * \brief Performs distance query and pass the internal id to a functor
             */
            template<typename Functor>
            inline void _doQueryDistIndicesWithFunctor(const VectorType& queryPoint, float sqdist, Functor f);
        protected:

            PointList  mPoints;
            IndexList  mIndices;
            AxisAlignedBoxType mAABB;
            NodeList   mNodes;
            QueryNode mNodeStack[64];

            unsigned int _nofPointsPerCell;
            unsigned int _maxDepth;
        public:
            const Mesh &mesh;
    };
    
}

#include "ClosetKdTreeSearch.hpp"
#endif