#ifndef __F3DRL_CLOSEST_LINEAR_SEARCH__
#define __F3DRL_CLOSEST_LINEAR_SEARCH__

namespace f3drl
{
    /**
     * @brief ClosestLinearSearch is the standar interface for your PointSet
     * he define the concept of finding the best less far point
     * we recommand to use ClosestKdTreeSearch to optimize the performance
     */
    template<class _InputSetType, template <class, unsigned> class _DistanceEstimator>
    struct ClosestLinearSearch : public _InputSetType
    {
        using Base = _InputSetType;
        enum { Dim = Base::Dim };
        
        using Point = typename Base::Point;
        using Scalar = typename Base::Scalar;
        using DistanceEstimatorType = _DistanceEstimator<Scalar, Dim>;
        
        /**
         * @return the best less far point to @other in this PointSet
         * iterate all Point in the PointSet and return the best Point
         */
        inline Point find(const Point &other) const noexcept
        {
            Point i = Base::points[0];
            double best = std::numeric_limits<double>::infinity();
            
            for(const auto it : Base::points)
            {
                float current = DistanceEstimatorType::length(it, other);
                if(current < best)
                    best = current, i = it;
            }
            
            return i;
        }
        
        /**
         * @return an approximated point to @other in this PointSet for performance
         * it's the first Point which is less than @max_distance
         */
        inline Point find(const Point &other, float max_distance) const noexcept
        {
            Point i = Base::points[0];
            
            for(const auto it : Base::points)
            {
                float current = DistanceEstimatorType::length(it, other);
                if(current < max_distance)
                    return it;
            }
        }
    };
}

#endif
