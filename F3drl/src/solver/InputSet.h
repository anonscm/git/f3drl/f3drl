#ifndef __SUPER4PCS_INPUTSET_WRAPPER__
#define __SUPER4PCS_INPUTSET_WRAPPER__

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/SVD>

#include <vector>

namespace f3drl
{
    /**
     * @brief define the set where the "Mesh" is stored
     * with an optional index storage for the triangle list
     *
     * and define all base type, like:
     *    - @Point (a vector in @Dim dimension et the same Scalar)
     *    - @PointList the point storage type, usuly an std::vector<Point>
     *    - @IndexList the index storage type, usuly an std::vector<std::size_t>
     * 
     * _Scalar = is our data type {int, float, double, ...}
     *  dimemsion = the dimemsion space of the data {1, 2, 3, 4, .... or Eigen::Dynamic}
     */
    template<typename _Scalar, unsigned dimension>
    class InputSet
    {
        public:
            enum { Dim = dimension };
            using Scalar = _Scalar;
            
            using Point = Eigen::Matrix<Scalar, Dim, 1>;
            using PointList = std::vector<Point, Eigen::aligned_allocator<Point>>;
            using IndexList = std::vector<std::size_t>;
        public:
            PointList points;
            IndexList indices;
    };
}

#endif