#ifndef __F3DRL_UTILITY__
#define __F3DRL_UTILITY__

namespace f3drl
{
    /**
     * @brief used to select the right reset function signature depending of the Iterator::IsCongruentIterator
     */
    template<typename Iterator, bool>
    struct IteratorSelectResetSignaturCall;
    
    template<typename Iterator>
    struct IteratorSelectResetSignaturCall<Iterator, true>
    {
        /**
         * reset signature call if Iterator::IsCongruentIterator is true
         */
        static void reset(Iterator &it, std::size_t base[Iterator::Size]) { it.reset(base); }
    };
    
    template<typename Iterator>
    struct IteratorSelectResetSignaturCall<Iterator, false>
    {
        /**
         * reset signature call if Iterator::IsCongruentIterator is false
         */
        static void reset(Iterator &it, std::size_t base[Iterator::Size]) { it.reset(); }
    };
    
    /**
     * @brief template alias of IteratorSelectResetSignaturCall
     */
    template<typename Iterator>
    struct resetcall : public IteratorSelectResetSignaturCall<Iterator, Iterator::IsCongruentIterator>{};
}

#endif