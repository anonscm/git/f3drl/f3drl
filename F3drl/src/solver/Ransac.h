#ifndef __F3DRL_RANSAC_ALGORITHM__
#define __F3DRL_RANSAC_ALGORITHM__

#include "Utility.h"

namespace f3drl
{
    template<
        typename _Base,                // InputSet would be decorated with an accelerator structure (or a least a ClosestLinearSearch)
        typename _Target,              // InputSet just need to define the template paramater
        typename _TransformEstimator,  // Estimate the inverse transform to fit a set of Point to an other set of Point
        typename _TransformEvaluator,  // Evaluate the fit score between Base and Target
        typename _TransformCallback,   // User defined, used to send information for validation
        typename _BaseIterator,        // TriangleIterator<Base::Dim>,
        typename _CongruentSetIterator // TriangleIterator<Target::Dim>
    >
    /**
     * @brief This is a fonctor based class to execute the Ransac algorithm:
     * you just need to "typedef" your custom Ransac,
     * instantiate it, and call the () operator
     *
     * - reset the base iterator -> iterate over "P"
     *    - save the base triangle matrix -> "in"
     *    - reset the target iterator  -> iterate over "Q"
     *    - filter the congrient iterator with the base iterator triangle index
     *        - save the target triangle matrix -> "out"
     *        - estimate the inverse affine transform between "in" and "out" -> "tr"
     *        - estimate the distance between "P" and "tr*Q" -> "score"
     *        - send the score and the transformation to the user callback
     *        
     * with the following input data:
     *      -BaseType the first InputSet where Point&Index is stored -> "P"
     *      -TargetType the second InputSet where Point&Index is stored -> "Q"
     *
     *      -TransformEstimator used to calculate the affine transform
     *       to fit a triangle from the Target to the Base
     *
     *      -TransformEvaluator used to calculate the fit score
     *       between two Base and Target set (distance) 
     *
     *      -TransformCallback an user defined fonctor class called each time
     *       the algorithm found an affine transform with a score
     *       this functor may store the "best" affine transform and return if
     *       the ransac main loop would continue.
     *
     *      -BaseIterator an another class functor
     *       used to access to the next Triangle of the BaseType
     *       this BaseIterator must be a base_iterator
     *
     *      -CongruentSetIterator an another class functor
     *       used to access to the next Triangle of the Target
     *       this CongruentSetIterator must be a base_iterator or a congruentset_iterator
     *
     * all of this template parameter can be user defined,
     * but we need to respect the interface (all fonction signature)
     * all given type and static value as a compilation check for security and consistency
     */
    class Ransac
    {
        public:
            using BaseType                 = _Base;
            using TargetType               = _Target;
            using TransformEstimatorType   = _TransformEstimator;
            using TransformEvaluatorType   = _TransformEvaluator;
            using TransformCallbackType    = _TransformCallback;
            using BaseIteratorType         = _BaseIterator;
            using CongruentSetIteratorType = _CongruentSetIterator;
            
            /*
             * set the local reference with the given parameter
             * and do a compilation check of the template parameter
             * nothing else
             */
            Ransac(
                const BaseType &P, const TargetType &Q,
                TransformCallbackType &signal,
                TransformEvaluatorType &eval,
                BaseIteratorType &pit,
                CongruentSetIteratorType &qit
            ) noexcept
                : p(P), q(Q), signal(signal), eval(eval), pit(pit), qit(qit)
            {
                static_assert(
                    std::is_same<typename BaseType::Scalar, typename TargetType::Scalar>::value &&
                    std::is_same<typename BaseType::Scalar, typename TransformEstimatorType::Scalar>::value &&
                    std::is_same<typename BaseType::Scalar, typename TransformEvaluatorType::Scalar>::value,
                    "Unconsistent Scalar types"
                );
                
                //struct A : public BaseType::DistanceEvaluatorType {};
                //struct B : public TransformEvaluator::DistanceEvaluatorType {};
                //static_assert(std::is_same<A, B>::value);
                
                // typecheck failed in with annonimous enum value of compile time typename type
                static constexpr const unsigned DimBas = BaseType::Dim;
                static constexpr const unsigned DimTrg = TargetType::Dim;
                static constexpr const unsigned TrEst = TransformEstimatorType::Dim;
                static constexpr const unsigned TrEvl = TransformEvaluatorType::Dim;
                
                static_assert(DimBas == DimTrg, "Unconsistent dimension values");
                static_assert(DimBas == TrEst, "Unconsistent dimension values");
                static_assert(DimBas == TrEvl, "Unconsistent dimension values");
                
                assert(p.points.size());
                assert(q.points.size());
            }
            
            /*
             * execute the Ransac algorithm,
             * when this algorithm, found an inversse transform
             * from the "target" and the "base", an Event call is submited
             * (with the score and the afine transform) to the TransformCallback
             *
             * TransformCallbackType check the validity and return
             * if the algotihm would continue to search an inversse transform
             * he may also store the score of a fit solution
             * and the solution to estimate the best one
             */
            inline void operator() () noexcept
            {
                static constexpr const unsigned Dim = BaseType::Dim;
                static constexpr const unsigned Size = BaseIteratorType::Size;
            
                TransformEstimatorType lookup;
                typename TransformEstimatorType::MatrixIO in(Dim, Size);
                typename TransformEstimatorType::MatrixIO out(Dim, Size);
                
                pit.reset();
                
                while(++pit)
                {
                    #ifdef __DEBUG
                    std::cout <<
                        pit[0] << "," <<
                        pit[1] << "," <<
                        pit[2] << std::endl;
                    #endif
                        
                    for(auto i = 0u; i<Size; ++i)
                        in.col(i) = p.points[pit[i]];
                        
                    resetcall<CongruentSetIteratorType>::reset(qit, pit.current);
                    
                    while(++qit)
                    {
                        for(auto i = 0u; i<Size; ++i)
                            out.col(i) = q.points[qit[i]];
                    
                        auto tr = lookup(out, in);
                        auto current = eval(p, q, tr);
                        
                        if(signal(tr, current))
                            return;
                    }
                }
            }
        protected:
            const BaseType &p;
            const TargetType &q;
        protected:
            TransformCallbackType &signal;
            TransformEvaluatorType &eval;
        protected:
            BaseIteratorType &pit;
            CongruentSetIteratorType &qit;
    };
}

#endif
