#ifndef __F3DRL_TRANSFORM_LCP_EVALUATOR__
#define __F3DRL_TRANSFORM_LCP_EVALUATOR__

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/SVD>

#include <vector>

namespace f3drl
{
    template<typename _Base, typename _Target, template <class, unsigned> class _DistanceEvaluator>
    /**
     * @brief this TransformEvaluator define an LCP distance estimator between two InputSet/Mesh (P and Q)
     * this evaluator calculate the number of transformed point in "Q" that is <b>not</b> in "P"
     * with a maximum distance lookup of @max_distance calculated by the DistanceEvaluator (like euclidian)
     *
     * negation is usualy to have the same behavior for all TransformEvaluator to return a "normalized" score
     * and have the same user defined TransformCallback (search to minimize the score/distance)
     *
     * the "nearest neighbor search" can be accelerated by BaseType
     * use this TransformLCPEvaluator as a functor
     */
    class TransformLCPEvaluator
    {
        public:
            enum { Dim = _Base::Dim };
            using BaseType = _Base;
            using TargetType = _Target;
            
            using Point = typename BaseType::Point;
            using Scalar = typename BaseType::Scalar;
            
            using Transform = Eigen::Transform<Scalar, Dim, Eigen::Affine>;
            using DistanceEvaluatorType = _DistanceEvaluator<Scalar, Dim>;
        public:
            TransformLCPEvaluator(float max_distance = 10.f)
                : max_distance(max_distance)
            {
            }
            
            /**
             * @brief Estimate the distance between "P" and "tr*Q"
             * return the number of transformed point in "Q" that is <b>not</b> in "P"
             * with a maximum distance lookup of @max_distance
             */
            inline double operator() (const BaseType &p, const TargetType &q, const Transform &tr) const noexcept
            {
                double acc = q.points.size();
                
                for(const auto &it : q.points)
                {
                    Point rec = (tr.matrix()*it.homogeneous()).template head<Dim>();
                    Point nns = p.find(rec, max_distance);
                    acc -= DistanceEvaluatorType::length(rec, nns) < max_distance;
                }
                
                // we search to maximize the number corelated point of Q in P
                // but we use ransac & functor for minimization, ...
                // so the minimum of the maximum would be Q_SIZE - MATCHED_POINT_SIZE
                
                return acc;
            }
            
            float max_distance;
    };
}

#endif
