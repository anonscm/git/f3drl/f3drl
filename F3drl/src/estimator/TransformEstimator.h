#ifndef __F3DRL_TRANSFORM_ESTIMATOR__
#define __F3DRL_TRANSFORM_ESTIMATOR__

#include <Eigen/Geometry>
#include <assert.h>

namespace f3drl
{
    /**
     * @brief This TransformEstimator estimate the invers affine transform
     * find the rotation + translation + scale which best maps the first set to the second.
     * to fit one triangle to an other, this is a Kabsch based algorithm
     * The input points are stored as columns.
     */
    template<typename _Scalar, unsigned dimension = 3>
    struct TransformEstimator
    {
        enum { Dim = dimension };
        using Scalar = _Scalar;
        
        using Transform = Eigen::Transform<Scalar, Dim, Eigen::Affine>;
        using MatrixIO = Eigen::Matrix<Scalar, Dim, Eigen::Dynamic>;
        using Matrix = Eigen::Matrix<Scalar, Dim, Dim>;
        using Vector = Eigen::Matrix<Scalar, Dim, 1>;
        
        /**
         * return the affine transform which best maps the @out set to @inset
         */
        inline Transform operator() (MatrixIO &in, MatrixIO &out) const noexcept
        {
            Transform A;
            A.linear() = Matrix::Identity(Dim, Dim);
            A.translation() = Vector::Zero();

            // First find the scale, by finding the ratio of sums of some distances,
            // then bring the datasets to the same scale.
            
            double dist_in = 0, dist_out = 0;
            for(int col = 0; col < in.cols()-1; col++) 
            {
                dist_in  += (in.col(col+1) - in.col(col)).norm();
                dist_out += (out.col(col+1) - out.col(col)).norm();
            }
            
            if(dist_in <= 0 || dist_out <= 0)
                return A;
            
            Scalar scale = dist_out/dist_in;
            out /= scale;

            // Find the centroids then shift to the origin
            Vector in_ctr = Vector::Zero();
            Vector out_ctr = Vector::Zero();
            
            for(int col = 0; col < in.cols(); col++)
            {
                in_ctr  += in.col(col);
                out_ctr += out.col(col);
            }
            
            in_ctr /= in.cols();
            out_ctr /= out.cols();
            
            for(int col = 0; col < in.cols(); col++)
            {
                in.col(col)  -= in_ctr;
                out.col(col) -= out_ctr;
            }

            // SVD
            Eigen::MatrixXf Cov = in * out.transpose();
            Eigen::JacobiSVD<Eigen::MatrixXf> svd(Cov, Eigen::ComputeThinU | Eigen::ComputeThinV);

            // Find the rotation
            Scalar d = (svd.matrixV() * svd.matrixU().transpose()).determinant();
            d = -1.0 + 2.0*(d > 0);
              
            Matrix I = Matrix::Identity(Dim, Dim);
            I(Dim-1, Dim-1) = d;
            
            Matrix R = svd.matrixV() * I * svd.matrixU().transpose();

            // The final transform
            A.linear() = scale * R;
            A.translation() = scale*(out_ctr - R*in_ctr);

            return A;
        }
    };
}
    
#endif