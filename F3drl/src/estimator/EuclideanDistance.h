#ifndef __F3DRL_EUCLIDIAN_DISTANCE__
#define __F3DRL_EUCLIDIAN_DISTANCE__

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/SVD>

#include <vector>

namespace f3drl
{
    /**
     * @brief This helper class define the operator to calcule the square euclidian distance between to vector
     */
    template<typename _Scalar, unsigned dimension>
    class EuclidianDistance
    {
        public:
            enum { Dim = dimension };
            using Scalar = _Scalar;
            using Point = Eigen::Matrix<Scalar, Dim, 1>;
        public:
            /**
             * return the square euclidian distance between @A and @B
             */
            static double length(const Point &A, const Point &B) noexcept
            {
                double sum = 0.0f;
                for(unsigned i = 0; i<Dim; ++i)
                    sum += (A(i,0)-B(i,0))*(A(i,0)-B(i,0));
                return sum;
            }
    };
}

#endif