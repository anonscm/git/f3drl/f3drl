#ifndef __F3DRL_TRANSFORM_EVALUATOR__
#define __F3DRL_TRANSFORM_EVALUATOR__

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/SVD>

#include <vector>

namespace f3drl
{
    /**
     * @brief this TransformEvaluator define an simple distance estimator between two InputSet/Mesh (P and Q)
     * it's usualy a sum of best Point-to-point distance
     *
     *      - search the nearest Point in "P" of all "tr*Q" point
     *      - accumulate the result
     *
     * the "nearest neighbor search" can be accelerated by BaseType
     * use this TransformEvaluator as a functor
     */
    template<typename _Base, typename _Target, template <class, unsigned> class _DistanceEvaluator>
    class TransformEvaluator
    {
        public:
            enum { Dim = _Base::Dim };
            using BaseType = _Base;
            using TargetType = _Target;
            
            using Point = typename BaseType::Point;
            using Scalar = typename BaseType::Scalar;
            
            using Transform = Eigen::Transform<Scalar, Dim, Eigen::Affine>;
            using DistanceEvaluatorType = _DistanceEvaluator<Scalar, Dim>;
        public:
            /**
             * return the sum of best Point-to-point distance
             * (the nearest Point in "P" of all "tr*Q" point)
             */
            inline double operator() (const BaseType &p, const TargetType &q, const Transform &tr) const noexcept
            {
                double acc = 0;
                
                for(const auto &it : q.points)
                {
                    Point rec = (tr.matrix()*it.homogeneous()).template head<Dim>();
                    Point nns = p.find(rec);
                    acc += DistanceEvaluatorType::length(rec, nns);
                }
                
                return acc;
            }
    };
}

#endif
