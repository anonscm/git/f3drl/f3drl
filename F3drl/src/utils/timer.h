#ifndef __F3DRL_TIMER__
#define __F3DRL_TIMER__

#include <chrono>
#include "F3drl/Core"

namespace f3drl
{
    /**
     * @brief a timer class to simplify to get the time distance between two piece of code ...
     */
    class Timer
    {
        public:
            typedef std::chrono::high_resolution_clock clock;
            typedef std::chrono::nanoseconds timestep;

            explicit inline Timer(bool run = false) noexcept
            {
                if(run)
                    reset();
            }
            
            /**
             * restart the timer for elapsed interval
             */
            inline void reset() noexcept
            {
                _start = clock::now();
            }
            
            /**
             * calculate the elapsed between the last @reset cal and now
             */
            inline timestep elapsed() const noexcept
            {
                return std::chrono::duration_cast<timestep>(clock::now() - _start);
            }

            template <typename T, typename Traits>
            friend std::basic_ostream<T, Traits>& operator<<(std::basic_ostream<T, Traits>& out, const Timer& timer) noexcept
            {
                return out << timer.elapsed().count();
            }
        private:
            clock::time_point _start;
    };
}

#endif
