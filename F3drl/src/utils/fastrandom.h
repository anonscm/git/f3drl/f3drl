#ifndef __F3DRL_FAST_RANDOM__
#define __F3DRL_FAST_RANDOM__

#include <cstddef>
#include <assert.h>

namespace f3drl
{
    /**
     * @brief a realy fast random generator without seed
     * you can use it, or a custom warpper in the engine,
     * the main utility is to generate a random index for iterators
     * like RandomIterator<> or RandomTriangleIterator<>
     */
    class fastrandom
    {
        public:
            inline fastrandom() noexcept
                : x(123456789), y(362436069), z(521288629)
            {
            }
            
            /**
             * Marsaglia's xorshf generator
             * with a period of 2^96-1,
             * generator and return the next number 
             */
            inline std::size_t operator() () noexcept
            {
                std::size_t t;
                
                x ^= x << 16;
                x ^= x >> 5;
                x ^= x << 1;

                t = x;
                x = y;
                y = z;
                z = t ^ x ^ y;

                return z;
            }
        private:
            std::size_t x, y, z;
    };
}

#endif