#ifndef __F3DRL_INDEX_ITERATOR__
#define __F3DRL_INDEX_ITERATOR__

#include <cstddef>

namespace f3drl
{
    /**
     * @brief simplified iterator for the vertices selection
     * used to replace the nested n-loop (1 per dimension)
     * with all different/unique value
     * 
     * you would use this helper in that way:
     * 
     * LinearPointIterator<3> it(0, mesh->size());
     * while(it++) {
     *     dostuff(
     *         mesh->points[it[0]],
     *         mesh->points[it[1]],
     *         mesh->points[it[2]]
     *     );
     * }
     * 
     * this helper must be optimised at compile time,
     * this this a prephase for vectices selection in N-Sizeension
     * with other purpose Fibonnaci, Kd-tree, Rand, ... with or
     * without Point filter, not yed defined
     * 
     * may be usefull latter to reconstruct the mesh indices
     * 
     * purpose like:
     * for(int i = 0; i<vertices; ++i)
     *     for(int j = 0; j<vertices; ++j + (i==j))
     *         for(int k = 0; k<vertices; ++k + (k==i) + (k == j))
     *             ...
     *                 for(int inf = 0; ..........; ++inf + sum of all previous equality)
     *                     dostuff(point[i], point[j], point[k], ..., point[inf]);
     *
     * <b>not a congruent iterator -> used for BaseType("P") or TargetType("Q")</b>
     */
    
    template<unsigned _D = 1u>
    class IndexIterator
    {
        public:
            enum { IsCongruentIterator = false };
            enum { Size = _D };
            
            /**
             * initialize all indices at zero
             * must be skiped with the right index vector
             * in the first iteration (duplicated or wrong triangle)
             */
            inline IndexIterator(std::size_t _end) noexcept : end(_end)
            {
                for(std::size_t i=0u; i<Size; ++i)
                    current[i] = 0u;
            }
            
            /**
             * calculate the next index vector
             */
            inline bool advance() noexcept
            {
                current[Size-1]++;
                
                for(std::size_t i = Size-1u; i-- > 0u;)
                    current[i] += current[i+1]/end;
                
                bool all = false;
                for(std::size_t i=0u; i<Size; ++i)
                    all |= (current[i] < end);
                    
                for(std::size_t i=1u; i<Size; ++i)
                    current[i] %= end;
                
                return all;
            }
            
            /**
             * calculate the next index vector / triangle indices
             * return false when all possible indices is generated [0,end-1]
             * automaticaly discard duplicated index
             * @see discard() function
             */
            inline bool operator++ () noexcept
            {
                bool end = advance();
                while(discard() && end)
                    end = advance();
                return end;
            }
            
            /**
             * detect when any index in the @current vector is greater or equals
             * this is simple loop optimisation:
             *      - discard duplicated triangle (twice)
             *      - discard collinear triangle (line)
             *      - discard "null" triangle (point)
             */
            inline bool discard() noexcept
            {
                bool all = true;
                for(std::size_t i=0u; i<Size-1; ++i)
                    all &= (current[i] > current[i+1]);
                return !all;
            }
            
            /**
             * return the index of the n-inner loop
             */
            inline std::size_t operator[] (std::size_t n) const noexcept
            {
                return current[n];
            }
            
            /**
             * set all index to 0
             * must be skiped with the right index vector
             * in the first iteration (duplicated or wrong triangle)
             */
            inline void reset() noexcept
            {
                for(std::size_t i=0u; i<Size; ++i)
                    current[i] = 0u;
            }
        public:
            std::size_t current[Size];
            std::size_t end;
    };
}

#endif