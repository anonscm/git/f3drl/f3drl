#ifndef __F3DRL_RANDOM_ITERATOR__
#define __F3DRL_RANDOM_ITERATOR__

#include <cstddef>
#include <assert.h>

namespace f3drl
{
    /**
     * @brief simplified iterator for the vertices selection
     * used to replace the nested n-loop (1 per dimension)
     * @see IndexIterator, same behavior but we randomly select the index
     * the number of iteration is defined by @end
     *
     * only discard all index vector which have any equals value
     *
     * <b> not a congruent iterator -> used for BaseType("P") or TargetType("Q") </b>
     */
     
    template<unsigned _D = 1u, class rand_gen = fastrandom>
    class RandomIterator
    {
        public:
            enum { IsCongruentIterator = false };
            enum { Size = _D };
            
            /**
             * @current initialized in the first operator ++ call
             * set the number of iteration by @end
             */
            inline RandomIterator(const std::size_t end)
                : start(0), end(end)
            {
            }
            
            /**
             * calculate the next index vector
             * use rand_gen the random functor generator
             */
            inline bool advance()
            {
                for(std::size_t i=0u; i<Size; ++i)
                    current[i] = frd() % end;
                return ++start < end;
            }
            
            /**
             * calculate the next index vector / triangle indices
             * @return false when all possible indices is generated [0,end-1]
             * automaticaly discard duplicated index (TODO ?)
             * @see discard() function
             */
            inline bool operator++ ()
            {
                bool end = advance();
                //while(discard() && end)
                //    end = advance();
                return end;
            }
            
            /**
             * discard all index vector which have any equals value:
             *      - discard collinear triangle (line)
             *      - discard "null" triangle (point)
             */
            inline bool discard()
            {
                bool all = false;
                for(std::size_t i=0u; i<Size-1; ++i)
                    all |= (current[i] == current[i+1]);
                return all;
            }
            
            /**
             * @return the index of the n-inner loop
             */
            inline std::size_t operator[] (const std::size_t i) const
            {
                return current[i];
            }
            
            /**
             * set all index to 0
             * must be skiped with the right index vector
             * in the first iteration (duplicated or wrong triangle)
             */
            inline void reset()
            {
                start = 0;
            }
        public:
            rand_gen frd;
            std::size_t current[Size];
            std::size_t start, end;
    };
}

#endif