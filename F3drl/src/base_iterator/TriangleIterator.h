#ifndef __F3DRL_TRIANGLE_ITERATOR__
#define __F3DRL_TRIANGLE_ITERATOR__

#include <cstddef>
#include <assert.h>

namespace f3drl
{
    /**
     * @brief simplified iterator for the vertices selection
     * used to replace the nested n-loop (1 per dimension)
     *
     * this iterator only iterate over the given @indices vector
     * we use and advance on offset to copy the first @Size at the current offset
     */
    template<unsigned _D = 1u>
    class TriangleIterator
    {
        public:
            enum { IsCongruentIterator = false };
            enum { Size = _D };
            
            /**
             * indices.size() % Size must be zero (congruent)
             * because the dimension must fit the data
             * @current initialized in the first operator ++ call
             */
            inline TriangleIterator(const std::vector<std::size_t> &indices)
                : indices(indices), index(0), end(indices.size())
            {
                assert(indices.size() % Size == 0);
                assert(indices.size() >= Size);
            }
            
            /**
             * advance the offset in the indices list
             * then copy the head to @current
             */
            inline bool operator++ ()
            {
                for(std::size_t i=0u; i<Size; ++i)
                    current[i] = indices[index+i];
                index += Size;
                
                return index < end;
            }
            
            /**
             * return the index of the n-inner loop
             */
            inline std::size_t operator[] (const std::size_t i) const
            {
                return current[i];
            }
            
            /**
             * only set the indices offset to zero
             */
            inline void reset()
            {
                index = 0;
            }
        public:
            const std::vector<std::size_t> &indices;
            std::size_t current[Size];
            std::size_t index;
            std::size_t end;
    };
}

#endif