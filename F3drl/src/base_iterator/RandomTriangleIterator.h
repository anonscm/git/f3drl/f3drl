#ifndef __F3DRL_RANDOM_TRIANGLE_ITERATOR__
#define __F3DRL_RANDOM_TRIANGLE_ITERATOR__

#include <cstddef>
#include <assert.h>

namespace f3drl
{
    /**
     * @brief simplified iterator for the vertices selection
     * used to replace the nested n-loop (1 per dimension)
     *
     * this iterator only iterate over the given @indices vector
     * we randomly select on offset to copy the first @Size at the current offset
     *
     * use rand_gen the random functor generator
     * and iterate @max time
     */
    template<unsigned _D = 1u, class rand_gen = fastrandom>
    class RandomTriangleIterator
    {
        public:
            enum { IsCongruentIterator = false };
            enum { Size = _D };
            
            /**
             * indices.size() % Size must be zero (congruent)
             * because the dimension must fit the data
             * @current initialized in the first operator ++ call
             */
            inline RandomTriangleIterator(const std::vector<std::size_t> &indices, int max)
                : indices(indices), index(0), end(max)
            {
                assert(indices.size() % Size == 0);
                assert(indices.size() >= Size);
                assert(indices.size() >= max);
                // @current initialized in the first operator ++ call
            }
            
            /**
             * calculate the next index vector / triangle indices
             * return false when all possible indices is generated [0,end-1]
             */
            inline bool operator++ ()
            {
                // random start index
                const std::size_t r = frd();
                const std::size_t k = r-r%Size;
                
                for(std::size_t i=0u; i<Size; ++i)
                    current[i] = indices[(k+i)%indices.size()];
                
                return ++index < end;
            }
            
            /**
             * return the index of the n-inner loop
             */
            template<std::size_t i> inline std::size_t get() const
            {
                return current[i];
            }
            
            /**
             * set all index to 0
             * must be skiped with the first iteration
             */
            inline void reset()
            {
                index = 0;
            }
        public:
            rand_gen frd;
            const std::vector<std::size_t> &indices;
            std::size_t current[Size];
            std::size_t index, end;
    };
}

#endif