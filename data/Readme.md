# expected result:

## ransac:

` model affine-original.obj expected result:
> affine-translation.obj:
  Matrix(((1,0,0,-2.98023e-08),(0,1,0,-1.43482),(0,0,1,-2.98023e-08),(0,0,0,1)))
  estimation: 1.20792e-13
  
> affine-rotate.obj:
  Matrix(((0.243625,0.785129,0.569401,2.98023e-07),(-0.150893,-0.549258,0.821916,4.76837e-07),(0.958059,-0.286158,-0.0153429,8.94069e-08),(0,0,0,1)))
  estimation: 3.19744e-13
  
> affine-scale.obj:
  Matrix(((0.358695,0,2.13799e-08,2.13799e-08),(0,0.358695,0,0),(-2.13799e-08,0,0.358695,-8.55195e-08),(0,0,0,1)))
  estimation: 1.42109e-14
  
> affine-rotate-scale.obj:
  Matrix(((0.657842,0.240181,-0.116127,-1.90404e-07),(-0.0557016,0.425849,0.565224,8.46242e-08),(0.260901,-0.514679,0.413478,-6.34681e-08),(0,0,0,1)))
  estimation: 1.64491e-12
  
> affine-rotate-scale-move.obj:
! rotate+scale ok, but not sur for translation ...
  Matrix(((2.01386,-0.541709,0.67224,1.86122),(0.837398,1.64073,-1.18649,-1.67186),(-0.210045,1.34742,1.71503,-2.35013),(0,0,0,1)))
  estimation: 6.74305e-12+
  
` model monkey1.obj expected result:
> monkey2.obj:
! best result see after some days of computation time
  Matrix(((-1.07043,0.545274,0.0963145,0.367473),(-0.551607,-1.03183,-0.288926,3.9015),(-0.0482619,-0.300708,1.16605,-2.11575),(0,0,0,1)))
  estimation: 698.304


## 4PCS:
  
` model hippo1.obj expected result:

> hippo2.obj:
  estimation:
  
` model monkey1.obj expected result:

> monkey2.obj:
  estimation:
  
` bla bla bla