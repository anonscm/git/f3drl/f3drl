#         TODO LIST    #

* [ ] cmake: implement some automatic test
* [ ] cmake: by default, compile with assimp (if present), else, use an internal object loader
* [ ] imnplement the kdtree acceleration structure
* [ ] imnplement the kdtree iterator for lcp
* [ ] imnplement lcp
* [ ] add congruent iterator
* [ ] add initFromBase(in) methode for congruent iterator

#           DONE       #

* [x] add iterator test
* [x] remove the iterator discard method
* [x] rename some class & member & template
      TransformEstimator, TransformEvaluator, DistanceEvaluator, Dim, ...
* [x] cmake
* [x] ransac
* [x] assimp loader

#   NEED SOME REWORK   #

* [ ] obj loader
* [x] read http://graphics.stanford.edu/~niloy/research/fpcs/fpcs_sig_08.html