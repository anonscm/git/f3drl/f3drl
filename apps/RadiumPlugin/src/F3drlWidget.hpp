#ifndef F3DRLWIDGET_H
#define F3DRLWIDGET_H

#include <QWidget>

namespace Ui {
class F3DRLWidget;
}

namespace F3DRLPlugin {
    class F3DRLWidget : public QWidget
    {
        Q_OBJECT

    public:
        struct Parameters {
            std::string target;
            std::string source;
        };

        explicit F3DRLWidget(QWidget *parent = 0);
        ~F3DRLWidget();

    private slots:
        void syncParamFromUI();
        void on__input3dmodelSource_currentIndexChanged(const QString &text);
        void on__input3dmodelTarget_currentIndexChanged(const QString &text);

    public slots:
        void addInputModel( const std::string& name);

    signals:
        void targetModelChanged( const std::string& name);
        void sourceModelChanged( const std::string& name);
        void parametersChanged( const Parameters& p);
        void registrationRequested(const Parameters& p);

    private:
        Ui::F3DRLWidget *ui;
        Parameters _param;
    };

} // namespace F3DRLPlugin

#endif // F3DRLWIDGET_H
