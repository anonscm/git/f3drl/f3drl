#ifndef F3DRLPLUGIN_HPP_
#define F3DRLPLUGIN_HPP_

#include <Core/CoreMacros.hpp>
/// Defines the correct macro to export dll symbols.
#if defined  DF3DRLPlugin_EXPORTS
    #define F3DRL_PLUGIN_API DLL_EXPORT
#else
    #define F3DRL_PLUGIN_API DLL_IMPORT
#endif

#include <QObject>
#include <QtPlugin>

#include <PluginBase/RadiumPluginInterface.hpp>

namespace Ra
{
    namespace Engine
    {
        class RadiumEngine;
    }
}

namespace F3DRLPlugin
{
    class F3DRLSystem;
    class F3DRLWidget;

// Du to an ambigous name while compiling with Clang, must differentiate plugin claas from plugin namespace
    class F3DRLPluginC : public QObject, Ra::Plugins::RadiumPluginInterface
    {
        Q_OBJECT
        Q_PLUGIN_METADATA( IID "RadiumEngine.PluginInterface" )
        Q_INTERFACES( Ra::Plugins::RadiumPluginInterface )

    public:
        F3DRLPluginC();
        virtual ~F3DRLPluginC();

        virtual void registerPlugin( const Ra::PluginContext& context ) override;

        virtual bool doAddWidget( QString& name ) override;
        virtual QWidget* getWidget() override;

        virtual bool doAddMenu() override;
        virtual QMenu* getMenu() override;

    private slots:
        void registerInputModel( const std::string& name);

    private:
        F3DRLSystem* _system;
        F3DRLWidget* _widget;

    };

} // namespace

#endif // F3DRLPLUGIN_HPP_
