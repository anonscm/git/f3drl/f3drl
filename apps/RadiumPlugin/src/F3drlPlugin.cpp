#include <F3drlPlugin.hpp>

#include <Engine/RadiumEngine.hpp>

#include <F3drlSystem.hpp>
#include <F3drlWidget.hpp>

namespace F3DRLPlugin
{

    F3DRLPluginC::F3DRLPluginC()
        : _system (nullptr),
          _widget (nullptr)
    {
    }

    F3DRLPluginC::~F3DRLPluginC()
    {
    }

    void F3DRLPluginC::registerPlugin( const Ra::PluginContext& context )
    {
        delete (_system);
        _system = new F3DRLSystem;
        context.m_engine->registerSystem( "F3DRLSystem", _system );
    }

    bool F3DRLPluginC::doAddWidget( QString &name )
    {
        delete (_widget);
        _widget = new F3DRLWidget();

        connect(_system, &F3DRLSystem::newInputModelRegistered,
                _widget, &F3DRLWidget::addInputModel);

        name = "3D Registration (F3DRL)";
        return true;
    }

    QWidget* F3DRLPluginC::getWidget()
    {
        return _widget;
    }

    bool F3DRLPluginC::doAddMenu()
    {
        return false;
    }

    QMenu* F3DRLPluginC::getMenu()
    {
        return nullptr;
    }

    void F3DRLPluginC::registerInputModel(const std::string &name)
    {
        _widget->addInputModel(name);
    }
}
