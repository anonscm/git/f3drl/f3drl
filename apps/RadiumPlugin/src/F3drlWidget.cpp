#include "F3drlWidget.hpp"
#include "ui_F3drlWidget.h"

#include <QComboBox>


namespace F3DRLPlugin {
    F3DRLWidget::F3DRLWidget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::F3DRLWidget)
    {
        ui->setupUi(this);
    }

    F3DRLWidget::~F3DRLWidget()
    {
        delete ui;
    }

    void F3DRLWidget::syncParamFromUI()
    {
        _param.source = ui->_input3dmodelSource->currentText().toStdString();
        _param.target = ui->_input3dmodelTarget->currentText().toStdString();
        emit parametersChanged(_param);
    }

    void F3DRLWidget::on__input3dmodelSource_currentIndexChanged(const QString &text)
    {
        emit sourceModelChanged(text.toStdString());
    }

    void F3DRLWidget::on__input3dmodelTarget_currentIndexChanged(const QString &text)
    {
        emit targetModelChanged(text.toStdString());
    }

    void F3DRLWidget::addInputModel(const std::string &name)
    {
        ui->_input3dmodelSource->addItem(QString::fromStdString(name));
        ui->_input3dmodelTarget->addItem(QString::fromStdString(name));
    }


} // namespace F3DRLPlugin
