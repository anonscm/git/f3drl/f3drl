#include <F3drlSystem.hpp>

#include <Core/String/StringUtils.hpp>
#include <Core/Tasks/Task.hpp>
#include <Core/Tasks/TaskQueue.hpp>

#include <Engine/RadiumEngine.hpp>
#include <Engine/Entity/Entity.hpp>
#include <Engine/FrameInfo.hpp>
#include <Engine/Renderer/RenderTechnique/RenderTechnique.hpp>
#include <Engine/Assets/FileData.hpp>
#include <Engine/Assets/GeometryData.hpp>
#include <Engine/Managers/ComponentMessenger/ComponentMessenger.hpp>

namespace F3DRLPlugin
{

    F3DRLSystem::F3DRLSystem()
        : Ra::Engine::System()
    {
        LOG(logINFO) << "Generating F3DRL system.";
    }

    F3DRLSystem::~F3DRLSystem()
    {
    }

    void F3DRLSystem::handleAssetLoading( Ra::Engine::Entity* entity, const Ra::Asset::FileData* fileData )
    {
        auto geomData = fileData->getGeometryData();

        uint id = 0;

        for ( const auto& data : geomData )
        {
            std::string componentName = "FMC_" + entity->getName() + std::to_string( id++ );
            LOG(logINFO) << "New entity: " << componentName.c_str();
            emit newInputModelRegistered(componentName);
        }
    }

    void F3DRLSystem::generateTasks( Ra::Core::TaskQueue* taskQueue, const Ra::Engine::FrameInfo& frameInfo )
    {
        // Do nothing, as this system only displays meshes.
    }

} // namespace F3DRLPlugin
