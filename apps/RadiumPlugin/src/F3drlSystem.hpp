#ifndef F3DRLPLUGIN_F3DRLPLUGIN_HPP_
#define F3DRLPLUGIN_F3DRLPLUGIN_HPP_

#include "F3drlPlugin.hpp"

#include <Engine/System/System.hpp>

namespace Ra
{
    namespace Core
    {
        struct TriangleMesh;
    }
}

namespace Ra
{
    namespace Engine
    {
        class Entity;
        struct RenderTechnique;
        class Component;
    }
}

namespace F3DRLPlugin
{
    class F3DRL_PLUGIN_API F3DRLSystem : public QObject, public Ra::Engine::System
    {
        Q_OBJECT
    public:
        F3DRLSystem();
        virtual ~F3DRLSystem();

        virtual void handleAssetLoading( Ra::Engine::Entity* entity, const Ra::Asset::FileData* fileData ) override;

        virtual void generateTasks( Ra::Core::TaskQueue* taskQueue, const Ra::Engine::FrameInfo& frameInfo ) override;

    signals:
        void newInputModelRegistered( const std::string& name );
    };

} // namespace F3DRLPlugin

#endif // F3DRLPLUGIN_F3DRLPLUGIN_HPP_
